var js2me = {
	debug: false,
	stat: 0,
	calledMethods: {},
	profile: false,
	storageName: '',
	libraryPath: 'js/me',
	constantPools: [], //only for memory profiling
	config: {
		app: true,
		engine: 'zazu',
		selector: false,
		workers: false,
		midlet: 1,
		width: 240,
		height: 266,
		fullHeight: 320
	}
};
js2me.BufferStream = function (buffer) {
	if (buffer.array) {
		this.array = buffer.array;
	} else {
		this.array = new Uint8Array(buffer);
	}
	this.index = 0;
};

js2me.BufferStream.prototype = {
	readUint8: function () {
		var byte = this.array[this.index];
		this.index++;
		return byte;
	},
	readUint16: function () {
		return this.readUint8() * 256 + this.readUint8();
	},
	readUint32: function () {
		return this.readUint16() * 65536 + this.readUint16();
	},
	readUint64: function () {
		return this.readUint32() * 65536 * 65536 + this.readUint32();
	},
	getSubstream: function (length) {
		return new js2me.BufferStream(this.array.subarray(this.index, this.index + length));
	},
	skip: function (length) {
		var pos = this.index;
		this.index += length;
		if (this.index > this.array.length) {
			this.index = this.array.length;
		}
		return this.index - pos;
	},
	getRemaining: function () {
		return this.array.length - this.index;
	},
	isEnd: function () {
		return this.index >= this.array.length;
	},
	seek: function (index) {
		this.index = index;
	},
	readInt16: function () {
		var value = this.readUint16();
		if (value > 32767) {
			value = value - 65536;
		}
		return value;
	},
	readInt8: function () {
		var value = this.readUint8();
		if (value > 127) {
			value = value - 256;
		}
		return value;
	},
	readInt32: function () {
		var value = this.readUint32();
		if (value > 2147483647) {
			value = value - 4294967296;
		}
		return value;
	},
	readInt64: function () {
		var value = this.readUint64();
		if (value > 9223372036854775807) {
			value = value - 18446744073709551616;
		}
		return value;
	},
	reset: function () {
		this.index = 0;
	}
};
(function () {
	lastFieldId = 1;
	
	function getNewClass() {
		return function () {
			if (this.construct) {
				this.construct();
			}
		};
	};
	
	/**
	 * Converts Java class definition into JavaScript.
	 * @param {BufferStream} stream The class file format compliant with JVM specifications.
	 * @return {constructor} JavaScript constructor of class.
	 */
	js2me.convertClass = function (stream) {
		var newClass = getNewClass();
		var constantPool = [];
		newClass.prototype.require = [];
		newClass.prototype.initialized = false;
		var TAG_UTF8 = 1;
		var TAG_INTEGER = 3;
		var TAG_FLOAT = 4;
		var TAG_LONG = 5;
		var TAG_DOUBLE = 6;
		var TAG_CLASS_INFO = 7;
		var TAG_STRING = 8;
		var TAG_FIELDREF = 9;
		var TAG_METHODREF = 10;
		var TAG_INTERFACEREF = 11;
		var TAG_NAME_AND_TYPE = 12;
		function checkHeader() {
			if (stream.readUint32() != 0xCAFEBABE) {
				throw new Error('Incorrect header');
			}
		}
		function checkVersion() {
			// Who cares...
			stream.readUint32();
		}
		function readConstantPool() {
			var count = stream.readUint16() - 1;
			for (var i = 1; i <= count; i++) {
				var tag = stream.readUint8();
				var constant = {
					tag: tag,
					implemented: false
				};
				if (tag == TAG_UTF8) {
					constant.implemented = true;
					var length = stream.readUint16();
					var bytes = [];
					for (var j = 0; j < length; j++) {
						bytes.push(stream.readUint8());
					}
					constant.value = js2me.UTF8ToString(bytes);
				}
				if (tag == TAG_INTEGER) {
					constant.implemented = true;
					constant.value = stream.readInt32();
				}
				if (tag == TAG_FLOAT) {
					constant.implemented = true;
					var value = stream.readUint32();
					constant.value = js2me.dataToFloat(value);
				}
				if (tag == TAG_LONG) {
					constant.implemented = true;
					constant.value = {hi: stream.readUint32(), lo: stream.readUint32()};
				}
				if (tag == TAG_DOUBLE) {
					constant.implemented = true;
					var hiData = stream.readUint32();
					var loData = stream.readUint32();
					constant.value = js2me.dataToDouble(hiData, loData);
				}
				if (tag == TAG_CLASS_INFO) {
					constant.implemented = true;
					constant.nameIndex = stream.readUint16();
				}
				if (tag == TAG_STRING) {
					constant.implemented = true;
					constant.stringIndex = stream.readUint16();
				}
				if (tag == TAG_FIELDREF || tag == TAG_METHODREF || tag == TAG_INTERFACEREF) {
					constant.implemented = true;
					constant.classIndex = stream.readUint16();
					constant.nameAndTypeIndex = stream.readUint16();
				}
				if (tag == TAG_NAME_AND_TYPE) {
					constant.implemented = true;
					constant.nameIndex = stream.readUint16();
					constant.descriptorIndex = stream.readUint16();
				}
				if (!constant.implemented) {
					throw new Error('Unimplemented tag ' + tag + ' at position ' + i);
				}
				constantPool[i] = constant;
				if (tag == TAG_LONG || tag == TAG_DOUBLE) {
					i++;
				}
			}
		}
		
		function escapeName(name) {
			name = '$' + name;
			if (name == '$<init>') {
				name = '_init';
			}
			if (name == '$<clinit>') {
				name = '_clinit';
			}
			return name;
		}
		
		var regExp = new RegExp('[\\[;/]', 'g')
		var regExpBrackets = new RegExp('[\\(\\)]', 'g')
		
		function escapeType(typeName) {
			typeName = typeName.replace(regExp, '_').replace(regExpBrackets, '$');
			return typeName;
		}
		
		function getArguments(typeName) {
			var args = typeName.slice(typeName.indexOf('(') + 1, typeName.indexOf(')'));
			var FIELD_TYPE = 0;
			var OBJECT_TYPE = 1;
			var ARRAY_TYPE = 2;
			var state = FIELD_TYPE;
			var argumentsTypes = [];
			var part = [];
			for (var i = 0; i < args.length; i++) {
				part.push(args[i]);
				if (state == FIELD_TYPE || state == ARRAY_TYPE) {
					if (args[i] == 'L') {
						state = OBJECT_TYPE;
					} else if (args[i] == '[') {
						state = ARRAY_TYPE;
					} else {
						argumentsTypes.push(part.join(''));
						part = [];
					}
				} else if (state == OBJECT_TYPE) {
					if (args[i] == ';') {
						state = FIELD_TYPE;
						argumentsTypes.push(part.join(''));
						part = [];
					}
				}
			}
			return argumentsTypes;
		}
		
		function resolveConstants() {
			function resolveConstant(index) {
				var constant = constantPool[index];
				var tag = constant.tag;
				if (tag == TAG_UTF8 || tag == TAG_INTEGER || tag == TAG_LONG || tag == TAG_FLOAT || tag == TAG_DOUBLE) {
					constantPool[index] = constant.value;
				}
				if (tag == TAG_CLASS_INFO) {
					var name = resolveConstant(constant.nameIndex);
					var isClass = true;
					var arrayPrefix = name.substr(0, name.lastIndexOf('[') + 1);
					name = name.substr(name.lastIndexOf('[') + 1);
					if (arrayPrefix.length > 0) {
						isClass = false;
					}
					if (name[0] === 'L' && name[name.length - 1] === ';') {
						isClass = true;
						name = name.substring(1, name.length - 1);
					}
					var nameElements = name.split('/');
					var className = ['javaRoot.'];
					for (var i in nameElements) {
						if (i > 0) {
							className.push('.');
						}
						className.push('$')
						className.push(nameElements[i]);
					}
					
					
					if (isClass) {
						//newClass.prototype.require.push(className.join(''));
					}
					className.unshift(arrayPrefix);
					constantPool[index] = {
						className: className.join('')
					};
				}
				if (tag == TAG_STRING) {
					var text = resolveConstant(constant.stringIndex);
					constantPool[index] = new javaRoot.$java.$lang.$String(text);
				}
				if (tag == TAG_FIELDREF || tag == TAG_METHODREF || tag == TAG_INTERFACEREF) {
					constantPool[index] = {
						className: resolveConstant(constant.classIndex).className,
						name: resolveConstant(constant.nameAndTypeIndex).name,
						type: resolveConstant(constant.nameAndTypeIndex).type
					};
					if (tag == TAG_METHODREF || tag == TAG_INTERFACEREF) {
						var constant = constantPool[index];
						var methodPath = [constant.className, '.prototype.', constant.name].join('');
						/*var methodPath = constant.className.replace('javaRoot.', '').replace('$', '');
						if (constant.name.indexOf('_init') === 0) {
							methodPath += '.<init>';
						} else {
							methodPath += '.' + constant.name.split('$')[1];
						}
						methodPath += '(';
						for (var t in constant.type.argumentsTypes) {
							var typeName = constant.type.argumentsTypes[t];
							var arrays = 0;
							while (typeName.indexOf('[') === 0) {
								arrays++;
								typeName = typeName.substring(1);
							}
							if (typeName === 'Z') {
								methodPath += 'boolean';
							} else if (typeName === 'I') {
								methodPath += 'int';
							} else if (typeName === 'B') {
								methodPath += 'byte';
							} else if (typeName === 'D') {
								methodPath += 'double';
							} else if (typeName === 'F') {
								methodPath += 'float';
							} else if (typeName === 'J') {
								methodPath += 'long';
							} else if (typeName === 'C') {
								methodPath += 'char';
							} else {
								methodPath += typeName.replace(/\//g, '.').substring(1, typeName.length - 1);
							}
							for (var tt = 0; tt < arrays; tt++) {
								methodPath += '[]';
							}
							if (t < constant.type.argumentsTypes.length - 1) {
								methodPath += ',';
							}
						}
						methodPath += ')';*/
						if (!js2me.config.app) {
							js2me.usedMethods[methodPath] = true;
						}
					}
				}
				if (tag == TAG_NAME_AND_TYPE) {
					var name = resolveConstant(constant.nameIndex);
					name = escapeName(name);
					var typeName = resolveConstant(constant.descriptorIndex);
					name += escapeType(typeName);
					
					constantPool[index] = {
						name: name,
						type: {
							argumentsTypes: getArguments(typeName),
							returnType: typeName.slice(typeName.indexOf(')') + 1)
						}
					};
				}
				return constantPool[index];
			}
			for (var i in constantPool) {
				resolveConstant(i);
			}
		}
		function readAccessFlags() {
			return stream.readUint16();
		}
		function readSuperClass() {
			newClass.prototype.superClass = constantPool[stream.readUint16()].className;
		}
		function readInterfaces() {
			newClass.prototype.interfaces = [];
			var count = stream.readUint16();
			for (var i = 0; i < count; i++) {
				newClass.prototype.interfaces.push(constantPool[stream.readUint16()].className);
			}
		}
		function readFields() {
			//TODO
			var count = stream.readUint16();
			for (var i = 0; i < count; i++) {
				var accessFlags = stream.readUint16();
				var name = constantPool[stream.readUint16()];
				var type = constantPool[stream.readUint16()];
				var attributes = readAttributes();
				var fieldName = escapeName(name) + escapeType(type);
				newClass.prototype[fieldName] = lastFieldId;
				newClass.prototype['$' + lastFieldId] = null;
				if (type == 'B' || type == 'S' || type == 'F' || type == 'I') {
					newClass.prototype['$' + lastFieldId] = 0;
				}
				if (type == 'D') {
					newClass.prototype['$' + lastFieldId] = js2me.dconst0;
				}
				if (type == 'J') {
					newClass.prototype['$' + lastFieldId] = {hi: 0, lo: 0}
				}
				if (type == 'C') {
					newClass.prototype['$' + lastFieldId] = 0;
				}
				if (type == 'Z') {
					newClass.prototype['$' + lastFieldId] = 0;
				}
				if (attributes['ConstantValue']) {
					newClass.prototype['$' + lastFieldId] = attributes['ConstantValue'];
				}
				js2me.statics['$' + lastFieldId] = newClass.prototype['$' + lastFieldId];
				lastFieldId++;
			}
		}
		function readAttributes(name, type, accessFlags) {
			var count = stream.readUint16();
			var attributes = {};
			for (var i = 0; i < count; i++) {
				var attributeName = constantPool[stream.readUint16()];
				var attributeLength = stream.readUint32();
				var value = null;
				if (attributeName == 'Code') {
					var maxStack = stream.readUint16();
					var maxLocals = stream.readUint16();
					var codeLength = stream.readUint32();
					var codeStream = stream.getSubstream(codeLength);
					stream.skip(codeLength);
					var exceptions = [];
					var exceptionTableLength = stream.readUint16();
					for (var j = 0; j < exceptionTableLength; j++) {
						exceptions[j] = {
							startPc: stream.readUint16(),
							endPc: stream.readUint16(),
							handler: stream.readUint16(),
							catchType: constantPool[stream.readUint16()]
						};
					}
					readAttributes(codeStream);
					var escapedName = escapeName(name) + escapeType(type);
					var argumentsTypes = getArguments(type);
					var methodName = thisClass.className + '.prototype.' + escapedName;
					//TODO: move it somewhere else
					value = js2me.generateMethodStub(newClass, codeStream, methodName, constantPool, exceptions, maxLocals, escapedName, argumentsTypes, accessFlags);
				}
				if (attributeName == 'Synthetic') {
					value = true;
				}
				if (attributeName == 'ConstantValue') {
					value = constantPool[stream.readUint16()];
				}
				if (attributeName == 'InnerClasses') {
					var classCount = stream.readUint16();
					value = [];
					for (var j = 0; j < classCount; j++) {
						var classInfo = {
							innerClass: constantPool[stream.readUint16()],
							outerClass: constantPool[stream.readUint16()],
							innerName: constantPool[stream.readUint16()],
							accessFlags: stream.readUint16()
						};
						value.push(classInfo);
					}
				}
				if (attributeName == 'Exceptions') {
					var exceptionsCount = stream.readUint16();
					value = [];
					for (var j = 0; j < exceptionsCount; j++) {
						value.push(constantPool[stream.readUint16()]);
					}
				}
				if (attributeName == 'StackMap' || attributeName == 'LineNumberTable' || attributeName == 'SourceFile' ||
					attributeName == 'LocalVariableTable') {
					//TODO: maybe it's needed by something?
					stream.skip(attributeLength);
					value = true;
				}
				if (value == null) {
					throw new Error('Unimplemented attribute ' + attributeName);
				}
				attributes[attributeName] = value;
			}
			return attributes;
		}
		function readMethods() {
			//TODO
			var count = stream.readUint16();
			for (var i = 0; i < count; i++) {
				var accessFlags = stream.readUint16();
				var name = constantPool[stream.readUint16()];
				var type = constantPool[stream.readUint16()];
				var attributes = readAttributes(name, type, accessFlags);
				newClass.prototype[escapeName(name) + escapeType(type)] = attributes['Code'];
			}
		}
		
		checkHeader();
		checkVersion();
		readConstantPool();
		resolveConstants();
		var accessFlags = readAccessFlags();
		if ((accessFlags & 0x0200) == 0) {
			newClass.prototype.type = 'class';
		} else {
			newClass.prototype.type = 'interface';
		}
		var thisClass = constantPool[stream.readUint16()];
		//js2me.constantPools.push(constantPool);
		newClass.prototype.className = thisClass.className;
		console.log('Converting class ' + thisClass.className);
		readSuperClass();
		readInterfaces();
		readFields();
		readMethods();
		readAttributes();
		return newClass;
	};
})();
(function () {
	/**
	 * Load multiple classes.
	 * @param {Array} classes List of class names.
	 * @param {function} callback Call after loading all of the classes.
	 */
	function loadClasses(classes, callback) {
		var threadId = js2me.currentThread;
		if (classes.length === 0) {
			callback();
			return;
		}
		var className = classes.pop();
		js2me.loadClass(className, function () {
			js2me.currentThread = threadId;
			loadClasses(classes, callback);
		}, function () {
			js2me.currentThread = threadId;
			loadClasses(classes, callback);
		})
		/*var remain = classes.length;
		function done() {
			remain--;
			if (remain === 0) {
				js2me.currentThread = threadId;
				callback()
			}
		}
		for (var i = 0; i < classes.length; i++) {
			js2me.loadClass(classes[i], done, done);
		}*/
	}
	var classLock = {};
	var blacklist = {};
	/**
	 * Loads a class (native or Javaish).
	 * @param {string} className Name of the class.
	 * @param {function(class)} callback Call after loading the class.
	 */
	js2me.loadClass = function (className, callback, errorCallback) {
		if (blacklist[className]) {
			errorCallback();
			return;
		}
		var error = null;
		var threadId = js2me.currentThread;
		
		if (classLock[className] && threadId !== classLock[className].threadId) {
			js2me.isThreadSuspended = true;
			classLock[className].waiting.push({
				threadId: threadId,
				successCallback: callback,
				errorCallback: errorCallback
			});
			return;
		}
		
		var classObj = js2me.findClass(className);
		if (classObj && (!js2me.statics || js2me.statics[className])) {
			callback(classObj);
		} else {
			console.log('Loading ' + className);
			classLock[className] = {
				threadId: threadId,
				waiting: []
			};
			
			function done(classObj, isError) {
				if (classLock[className]) {
					for (var i in classLock[className].waiting) {
						var lock = classLock[className].waiting[i];
						(function (lock) {
							js2me.restoreStack[lock.threadId].unshift(function () {
								js2me.loadClass(className, lock.successCallback, lock.errorCallback);
							});
							setTimeout(function () {
								js2me.restoreThread(lock.threadId);
							}, 0);
						})(lock);
					}
					delete classLock[className];
				}
				js2me.switchVM(threadId);
				if (classObj) {
					callback(classObj);
				} else {
					errorCallback();
				}
			}
			
			function loadDependecies() {
				js2me.currentThread = threadId;
				package[className.substr(className.lastIndexOf('.') + 1)] = classObj;
				if (classObj.prototype.interfaces instanceof Array) {
					require = require.concat(classObj.prototype.interfaces);
				}
				if (classObj.prototype.require instanceof Array) {
					require = require.concat(classObj.prototype.require);
					//delete classObj.prototype.require;
				}
				if (classObj.prototype.superClass) {
					require.push(classObj.prototype.superClass);
				}
				loadClasses(require, function () {
					js2me.restoreStack[threadId].unshift(function () {
						initializeClass(classObj, done);
					});
					js2me.restoreThread(threadId);
				});
			}
			
			var resourceName = className.replace('javaRoot.$', '').replace(/\.\$/g, '/') + '.class';
			var package = js2me.findPackage(className.substr(0, className.lastIndexOf('.')));
			js2me.isThreadSuspended = true;
			var require = [];
			if (!classObj) {
				if (js2me.resources[resourceName]) {
					js2me.loadResource(resourceName, function (data) {
						classObj = js2me.convertClass(new js2me.BufferStream(data));
						// lesser memory usage
						delete js2me.resources[resourceName];
						loadDependecies();
					});
				} else {
					var classPath = className.replace('javaRoot', js2me.libraryPath).replace(/\$/g, '').replace(/\./g, '/') + '.js';
					js2me.loadScript(classPath, function () {
						if (js2me.classBucket == null) {
							throw new javaRoot.$java.$lang.$ClassNotFoundException(className + ' not found');
						}
						var proto = js2me.classBucket.prototype;
						proto.className = className;
						classObj = js2me.classBucket;
						
						js2me.classBucket = null;
						loadDependecies();
					}, function () {
						console.error('Error loading ' + className + ' class.');
						blacklist[className] = true;
						done();
					});
				}
			} else {
				if (!js2me.statics[classObj.prototype.className]) {
					js2me.statics[classObj.prototype.className] = 1;
					loadDependecies();
				} else {
					done();
				}
			}
			
			
		}
		if (error) {
			throw error;
		}
	}
	/**
	 * Finds a package by its name.
	 * @param {string} path Package name.
	 * @param {object} [current=window] Entry point to search
	 * @return {object} Package object.
	 */
	js2me.findPackage = function (path, current) {
		if (!current) {
			current = window;
		}
		if (!path) {
			return current;
		}
		var name = path.substr(0, path.indexOf('.')) || path;
		if (!current[name]) {
			current[name] = {};
		}
		if (path.indexOf('.') > 0) {
			return js2me.findPackage(path.substr(path.indexOf('.') + 1), current[name]);
		} else {
			return current[name];
		}
	};
	var classCache = {};
	/**
	 * Finds a class by its full name.
	 * @param {string} path Class name.
	 * @return {constructor} Class constructor.
	 */
	js2me.findClass = function (path) {
		if (classCache[path] != null) {
			return classCache[path];
		}
		var package = this.findPackage(path.substr(0, path.lastIndexOf('.')));
		var classObj = package[path.substr(path.lastIndexOf('.') + 1)];
		/*if (!classObj) {
			throw new javaRoot.$java.$lang.$ClassNotFoundException(path);
		}*/
		classCache[path] = classObj;
		return classObj;
	};
	/**
	 * Finds classes which implement given class (including that class).
	 * @param {string} path Class name.
	 * @return {[constructor]} Class constructors.
	 */
	js2me.findSubclasses = function (path) {
		var result = [];		
		iterateClasses(javaRoot, 'javaRoot', function (classObj) {
			if (classObj instanceof Function && classObj.prototype.isImplement(path)) {
				result.push(classObj);
			}
		});
		return result;
	};
	/**
	 * Creates a constructor from given prototype and put into class bucket (i. e. result of executing current js file).
	 * @param {object} proto Object which is used as prototype.
	 */
	js2me.createClass = function (proto) {
		var classObj = function () {
			if (proto.construct) {
				proto.construct.apply(this, arguments);
			}
		};
		classObj.prototype = proto;
		proto.type = 'class';
		proto.initialized = false;
		js2me.classBucket = classObj;
	};
	/**
	 * Same as createClass but also change type into "interface".
	 * @param {object} proto Object which is used as prototype.
	 */
	js2me.createInterface = function (proto) {
		js2me.createClass(proto);
		proto.type = 'interface';
	};
	/**
	 * Iterates over some classes and packages and call a given function for each.
	 * @param {object} obj Entry point for iteration (use javaRoot if you want to iterate over whole space).
	 * @param {string} name Global name of entry point.
	 * @param {function(obj, name)} action Function to be executed for each found class and package.
	 */
	function iterateClasses(obj, name, action) {
		action(obj, name);
		for (var i in obj) {
			if (i[0] == '$') {
				iterateClasses(obj[i], name + '.' + i, action);
			}
		}
	};
	/**
	 * Initializes given class.
	 * @param {constructor} classObj Class constructor.
	 * @param {function(class)} callback Function to execute when class is ready to use.
	 */
	function initializeClass(classObj, callback) {
		console.log('Initializing ' + classObj.prototype.className);
		
		function markAsInitialized() {
			classObj.prototype.initialized = true;
			if (js2me.statics) {
				js2me.statics[classObj.prototype.className] = 2;
			}
		}
		
		if (classObj.prototype._clinit$$V && classObj.prototype.initialized && js2me.statics[classObj.prototype.className] !== 2) {
			classObj.prototype._clinit$$V(function () {
				markAsInitialized()
				callback(classObj);
			});
			return;
		}
		
		if (classObj === javaRoot.$java.$lang.$Object || classObj.prototype.initialized) {
			markAsInitialized()
			callback(classObj);
			return;
		}
		
		if (!classObj.prototype.superClass) {
			classObj.prototype.superClass = 'javaRoot.$java.$lang.$Object';
		}
		var superClass = js2me.findClass(classObj.prototype.superClass);
		//classObj.prototype.__proto__ = superClass.prototype;
		for (var i in superClass.prototype) {
			if (!classObj.prototype.hasOwnProperty(i)) {
				classObj.prototype[i] = superClass.prototype[i];
			}
		}
		
		if (classObj.prototype._clinit$$V) {
			markAsInitialized()
			classObj.prototype._clinit$$V(function () {
				callback(classObj);
			});
		} else {
			markAsInitialized()
			callback(classObj);
		}
	};
	
	/**
	 * Check if all used methods are implemented. Quite useless with lazy loading...
	 */
	js2me.checkMethods = function () {
		for (var methodPath in js2me.usedMethods) {
			// yeah, yeah, I know...
			var ref = eval(methodPath.replace('->', '.prototype.'));
			if (ref == null) {
				console.log('Method not found: ' + methodPath);
			}
		}
	};
	
	/**
	 * Prepares a JVM to usage. Basicaly loads some basic classes and sets initial state.
	 * @param {function} callback Function to execute when machine is ready.
	 */
	js2me.setupJVM = function (callback) {
		js2me.resources = {};
		js2me.threads = [];
		js2me.currentThread = 0;
		js2me.restoreStack = [[]];
		js2me.kill = false;
		js2me.usedMethods = {};
		js2me.usedByteCodes = {};
		javaRoot = {};
		var standardClasses = [
			'javaRoot.$java.$lang.$String',
			'javaRoot.$java.$lang.$Class',
			'javaRoot.$java.$lang.$Thread',
			'javaRoot.$java.$lang.$ClassNotFoundException',
			'javaRoot.$java.$lang.$ClassCastException',
			'javaRoot.$java.$lang.$ArrayIndexOutOfBoundsException',
			'javaRoot.$java.$lang.$NegativeArraySizeException',
			'javaRoot.$java.$lang.$ArrayObject',
			'javaRoot.$java.$lang.$ArithmeticException',
			'javaRoot.$java.$lang.$ArrayStoreException',
			'javaRoot.$java.$lang.$NullPointerException',
			'javaRoot.$javax.$microedition.$lcdui.$Display',
			'javaRoot.$javax.$microedition.$lcdui.$Displayable',
			'javaRoot.$javax.$microedition.$lcdui.$Screen',
			'javaRoot.$javax.$microedition.$lcdui.$Form',
			'javaRoot.$javax.$microedition.$lcdui.$StringItem',
			'javaRoot.$javax.$microedition.$lcdui.$ImageItem'
		];
		js2me.loadClass('javaRoot.$java.$lang.$Object', function () {
			loadClasses(standardClasses, callback);
		});
	};
})();
/**
 * Executes given program.
 * program An object returned by js2me.generateProgram
 * locals An array of local variables
 * constantPool 
 * exceptions
 * restoreInfo
 * callback
 */
js2me.execute = function (program, locals, constantPool, exceptions, restoreInfo, callback) {
	var context = {
		stack: [],
		result: null,
		locals: locals,
		position: 0,
		finish: false,
		saveResult: false,
		constantPool: constantPool,
		parameters: program.parameters
	};
	js2me.isThreadSuspended = false;
	if (restoreInfo) {
		context = restoreInfo.context;
		context.finish = false;
		callback = restoreInfo.callback;
		try {
			var result = js2me.restoreThread(js2me.currentThread);
			if (js2me.isThreadSuspended) {
				suspendCall();
			} else {
				if (context.saveResult) {
					context.stack.push(result);
					context.saveResult = false;
				}
			}
		} catch (exception) {
			tryCatchException(exception);
		}
		
	}
	
	function suspendCall() {
		if (js2me.restoreStack[js2me.currentThread] == null) {
			js2me.restoreStack[js2me.currentThread] = [];
		}
		var restoreStack = js2me.restoreStack[js2me.currentThread];
		restoreStack.push([program, locals, constantPool, exceptions, { 
			context: context,
			callback: callback
		}]);
		context.finish = true;
	}
	function tryCatchException(exception) {
		var handler = -1;
		for (var i = 0; i < exceptions.length && handler == -1; i++) {
			if (exceptions[i].startPc <= context.position - 1 && exceptions[i].endPc >= context.position - 1) {
				var obj = exception;
				var run = true;
				while (run) {
					if (exceptions[i].catchType == null || exceptions[i].catchType.className == obj.className) {
						handler = exceptions[i].handler;
						run = false;
					}
					if (obj.superClass) {
						obj = js2me.findClass(obj.superClass).prototype;
					} else {
						run = false;
					}
				}
			}
		}
		if (handler >= 0) {
			context.stack.push(exception);
			context.position = handler;
		} else {
			if (callback != null) {
				callback(exception);
			}
			throw exception;
		}
	}

	var length = program.content.length;
	while (context.position < length && !context.finish) {
		try {
			var func = program.content[context.position];
			context.position++;
			func(context);
		} catch (exception) {
			tryCatchException(exception);
		}
		
		if (js2me.isThreadSuspended) {
			suspendCall();
		}
		
	}
	if (context.regenerate) {
		program.regenerate = true;
	}
	if (callback != null && !js2me.isThreadSuspended) {
		callback(context.result);
	}
	
	return context.result;
};
js2me.listeners = {
	keypress: [],
	keyreleased: []
};
js2me.sendEvent = function (eventName, data) {
	for (var i = 0; i < this.listeners[eventName].length; i++) {
		(function (listener) {
			js2me.currentVM = 1;
			js2me.launchThread(function () {
				listener(data);
			});
		})(js2me.listeners[eventName][i]);
	}
};
js2me.addEventListener = function (eventName, listener) {
	this.listeners[eventName].push(listener);
};
js2me.removeEventListener = function (eventName, listener) {
	var pos = this.listeners[eventName].indexOf(listener);
	if (pos != -1) {
		this.listeners[eventName].splice(pos, 1);
	}
};
js2me.sendKeyPressEvent = function (keyCode) {
	this.sendEvent('keypress', keyCode);
};
js2me.sendKeyReleasedEvent = function (keyCode) {
	this.sendEvent('keyreleased', keyCode);
};
/**
 * Creates manifest object from given content.
 * @param {string} content Manifest file's content.
 * @return {object} Manifest object.
 */
js2me.parseManifest = function (content) {
	var manifest = {};
	var lines = content.split('\n');
	var lastSection;
	for (var i in lines) {
		var parts = lines[i].split(':');
		if (parts.length > 1) {
			lastSection = parts[0].toLowerCase();
			manifest[lastSection] = parts[1].trim();
		} else {
			manifest[lastSection] += parts[0].trim();
		}
	}
	return manifest;
}
js2me.generateMethodStub = function(newClass, stream, methodName, constantPool, exceptions, maxLocals, escapedName, argumentsTypes, accessFlags) {
	var data = {
		stream: new js2me.BufferStream(stream), 
		methodName: methodName,
		constantPool: constantPool,
		exceptions: exceptions,
		parent: newClass,
		name: escapedName,
		maxLocals: maxLocals,
		argumentsTypes: argumentsTypes,
		isStatic: (accessFlags & 8) !== 0,
		isSynchronized: (accessFlags & 0x20) !== 0
	};
	constantPool = undefined;
	stream = undefined;
	var stub = function (arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, guard) {
		
		var locals = [];
		if (!data.isStatic) {
			locals.push(this);
		}
		if (guard !== undefined) {
			console.error('Too many arguments');
		}
		if (js2me.currentThread === -10) {
			console.debug(data.methodName);
		}
		var args = [arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9];
		for (var i = 0; i < argumentsTypes.length; i++) {
			locals.push(args[i]);
			if (args[i] && (args[i].double != null || args[i].hi != null)) {
				locals.push(args[i]);
			}
		}
		var callback = null;
		if (args[argumentsTypes.length] && args[argumentsTypes.length].constructor === Function) {
			callback = args[argumentsTypes.length];
		}
		var result;
		if (data.content == null || data.regenerate) {
			js2me.generateProgram(data);
		}
		if (data.isSynchronized) {
			var l = this.monitorQueue ? this.monitorQueue.length : 0;
			var callee = this;
			
			js2me.enterMonitor(callee);
			var oldCallback = callback || function () {};
			callback = function () {
				js2me.exitMonitor(callee);
				oldCallback();
			}
			if (js2me.isThreadSuspended) {
				js2me.suspendThread(function () {
					return js2me.execute(data, locals, data.constantPool, exceptions, null, callback);
				});
				return;
			}	
		}
		if (data.nativeMethod) {
			return data.nativeMethod.apply(this, args);
		} else {
			return js2me.execute(data, locals, data.constantPool, exceptions, null, callback);
		}
	};
	stub.isUnsafe = !localStorage.getItem(js2me.storageName + methodName);
	stub.data = data;
	return stub;
};
js2me.generateAllMethods = function (force) {
	var generated = 0;
	for (var i in js2me.usedMethods) {
		var separator = i.indexOf('.prototype.');
		var className = i.substr(0, separator);
		try {
			var classObj = js2me.findClass(className);
		} catch (e) {}
		var methodName = i.substr(separator + 11);
		var data;
		if (classObj && classObj.prototype[methodName] && (data = classObj.prototype[methodName].data)) {
			if (data.content == null || data.regenerate || force) {
				js2me.generateProgram(classObj.prototype[methodName].data);
				generated++;
			}
		}
	}
	console.log(generated + ' methods have been generated.');
	return generated;
};
/**
 * Launches a midlet with given id.
 * @param {number} id Midlet's id.
 */
js2me.launchMidlet = function (id) {
	var mainMidlet = js2me.manifest['midlet-' + id];
	js2me['statics' + id] = {};
	var mainClassName = 'javaRoot.$' + mainMidlet.split(',')[2].trim().replace(/\./g, '.$');
	js2me.currentVM = id;
	var mainThread = new javaRoot.$java.$lang.$Thread(function () {
		js2me.loadClass(mainClassName, function (mainClass) {
			var midlet = new mainClass();
			midlet._init$$V(function () {
				midlet.$startApp$$V();
			});
		});
	});
	setTimeout(function () {
		js2me.currentVM = id;
		mainThread.$start$$V();
	}, 100);
};
js2me.launchJAR = function (blob) {
	if (js2me.config.workers) {
		js2me.worker.postMessage(['launchJAR', blob]);
	} else {
		js2me.loadScript(js2me.engine, function () {
			js2me.loadJAR(blob, function () {
				js2me.launchMidlet(js2me.config.midlet);
				if (js2me.manifest['nokia-midlet-bg-server']) {
					setTimeout(function () {
						js2me.launchMidlet(js2me.manifest['nokia-midlet-bg-server']);
					}, 0);
				}
			});
		});
	}
};
/**
 * Loads classes and resources from a given JAR file. This function purges state of JVM.
 * @param {string} filename Path to JAR file.
 * @param {function} callback Function to execute when loading is over.
 */
js2me.loadJAR = function (file, callback) {
	function loadReader(reader) {
		zip.useWebWorkers = false;
		zip.workerScriptsPath = 'js/zip/';
		zip.createReader(reader, function(zipReader) {
			zipReader.getEntries(function (entries) {
				js2me.addResources(entries)
				js2me.loadResource('META-INF/MANIFEST.MF', function (data) {
					var content = js2me.UTF8ToString(data);
					js2me.manifest = js2me.parseManifest(content);
					js2me.storageName = js2me.manifest['midlet-vendor'] + '/' +js2me.manifest['midlet-name'] + '//' + file.size + '/';
					callback();
				});
			});
		}, function (message) {
			js2me.showError(message);
		});
	}
	
	js2me.setupJVM(function () {
		loadReader(new zip.BlobReader(file));
	});
}
js2me.loadScript = function (filename, successCallback, errorCallback) {
	var element = document.createElement('script');
	element.src = filename;
	element.onload = function () {
		document.head.removeChild(element);
		//delete element.onload;
		//delete element.onerror;
		//element = undefined;
		successCallback();
	};
	element.onerror = function () {
		document.head.removeChild(element);
		//delete element.onload;
		//delete element.onerror;
		//element = undefined;
		errorCallback();
	};
	document.head.appendChild(element);
};
"use strict";
js2me.longToString = function (a) {
	var digits = [];
	var sign = false;
	for (var i = 0; i < 22; i++) {
		digits[i] = 0;
	}
	if (a.hi < 2147483648) {

		var hi = a.hi;
		var i = 0;
		while (hi > 0) {
			digits[i] = hi % 10;
			hi = Math.floor(hi / 10);
			i++;
		}
		for (var i = 0; i < 32; i++) {
			var rest = 0;
			for (var j = 0; j < digits.length; j++) {
				digits[j] *= 2;
				digits[j] += rest;
				rest = Math.floor(digits[j] / 10);
				digits[j] = digits[j] % 10;
			}
		}
		var lo = a.lo;
		var rest = 0;
		for (var i = 0; i < digits.length; i++) {
			digits[i] += lo % 10 + rest;
			rest = Math.floor(digits[i] / 10);
			digits[i] = digits[i] % 10;
			lo = Math.floor(lo / 10);
		}
	} else {
		sign = true;
		var hi = -(a.hi - 4294967296);
		var i = 0;
		while (hi > 0) {
			digits[i] = hi % 10;
			hi = Math.floor(hi / 10);
			i++;
		}
		for (var i = 0; i < 32; i++) {
			var rest = 0;
			for (var j = 0; j < digits.length; j++) {
				digits[j] *= 2;
				digits[j] += rest;
				rest = Math.floor(digits[j] / 10);
				digits[j] = digits[j] % 10;
			}
		}
		var lo = a.lo;
		var rest = 0;
		for (var i = 0; i < digits.length; i++) {
			digits[i] -= lo % 10 + rest;
			rest = 0
			while (digits[i] < 0) {
				digits[i] += 10;
				rest++;
			}
			lo = Math.floor(lo / 10);
		}
	}
	var last = 0;
	for (var i = 0; i < digits.length; i++) {
		if (digits[i] > 0) {
			last = i;
		}
		digits[i] += 48;
	}
	digits = digits.slice(0, last + 1);
	if (sign) {
		digits.push(45);
	}
	return digits.reverse();
};
js2me.ladd = function (a, b) {
	var result = {hi: 0, lo: 0};
	result.lo = a.lo + b.lo;
	result.hi = Math.floor(result.lo / 0x100000000)
	result.lo = result.lo % 0x100000000;
	result.hi += a.hi + b.hi;
	result.hi = result.hi % 0x100000000;
	return result;
};
js2me.lcmp = function (a, b) {
	function normalize(x) {
		if (x >= 0x80000000) {
			return x -= 0x100000000;
		} else {
			return x;
		}
	}
	if (a.hi === b.hi && a.lo === b.lo) {
		return 0;
	}
	if (normalize(a.hi) > normalize(b.hi)) {
		return 1;
	}
	if (normalize(a.hi) === normalize(b.hi) && normalize(a.lo) > normalize(b.lo)) {
		return 1;
	}
	return -1;
};
js2me.ldiv = function (t, b) {
	var result = {hi: 0, lo: 0};
	var sign = false;
	var a = {hi: t.hi, lo: t.lo};
	if (t.hi >= 2147483648) {
		sign = !sign;
		a = js2me.lneg(t);
	}
	if (t.hi >= 2147483648) {
		sign = !sign;
		b = js2me.lneg(b);
	}
	var c = b;
	var base = {hi: 0, lo: 1};
	while (js2me.lcmp(c, a) != 1 && c.hi < 0x80000000) {
		c = js2me.lshl(c, 1);
		base = js2me.lshl(base, 1);
	}
	while (js2me.lcmp(a, b) != -1) {
		c.lo = Math.floor(c.lo / 2);
		if (c.hi % 2 === 1) {
			c.lo += 0x80000000;
		}
		c.hi = Math.floor(c.hi / 2);
		base = js2me.lshr(base, 1);
		if (js2me.lcmp(c, a) != 1) {
			a = js2me.lsub(a, c);
			result = js2me.ladd(result, base);
		}
	}
	if (sign) {
		result = js2me.lneg(result);
	}
	if (t.hi >= 2147483648) {
		a = js2me.lneg(a);
	}
	return {
		div: result,
		rem: a
	};
};
js2me.lmul = function (a, b) {
	var result = {hi: 0, lo: 0};
	var sign = false;
	var c = {hi: a.hi, lo: a.lo};
	if (a.hi >= 2147483648) {
		sign = !sign;
		c = js2me.lneg(a);
	}
	if (b.hi >= 2147483648) {
		sign = !sign;
		b = js2me.lneg(b);
	}
	result.hi = (c.hi * b.lo + c.lo * b.hi);
	result.lo = c.lo * b.lo;
	var rest = Math.floor(result.lo / 0x100000000);
	result.hi = (result.hi + rest) % 0x100000000;
	result.lo = result.lo % 0x100000000;
	if (sign) {
		result = js2me.lneg(result);
	}
	return result;
};
js2me.lneg = function (a) {
	var result = {hi: 0, lo: 0};
	if (a.hi == 0 && a.lo == 0) {
		return result;
	}
	result.hi = 0xFFFFFFFF - a.hi;
	result.lo = 0xFFFFFFFF - a.lo;
	result.lo++;
	if (result.lo >= 0x100000000) {
		result.hi += Math.floor(result.lo / 0x100000000);
		result.lo = result.lo % 0x100000000;
	}
	return result;
};
js2me.lshl = function (a, shift) {
	var lo = a.lo;
	var hi = a.hi;
	for (var i = 0; i < shift; i++) {
		lo *= 2;
		hi *= 2;
	}
	var rest = Math.floor(lo / 0x100000000);
	lo = lo % 0x100000000;
	hi = hi % 0x100000000 + rest;
	return {hi: hi, lo: lo};
};
js2me.lshr = function (a, shift, isArithmetic) {
	if (shift === 0) {
		return a;
	}
	var lo = a.lo;
	var hi = a.hi;
	var base = Math.pow(2, shift);
	var fillWithOnes = false;
	
	if (isArithmetic && hi >= 0x80000000) {
		fillWithOnes = true;
	}
	
	hi = Math.floor(hi / base);
	if (fillWithOnes) {
		hi += 0xFFFFFFFF - Math.pow(2, 32 - shift) + 1;
	}
	lo = Math.floor(lo / base);
	var rest = a.hi % base;
	rest *= Math.pow(2, 32 - shift);
	lo += rest;
	return {hi: hi, lo: lo};
};
js2me.lsub = function (a, b) {
	var x = a.hi;
	if (x >= 0x80000000) {
		x -= 0x100000000;
	}
	var y = b.hi;
	if (y >= 0x80000000) {
		y -= 0x100000000;
	}
	var hi = (x - y);
	var lo = a.lo - b.lo;
	while (lo < 0) {
		lo += 0x100000000;
		hi--;
	}
	while (hi < 0) {
		hi += 0x100000000;
	}
	
	return {hi: hi, lo: lo};
};
js2me.lxor = function (a, b) {
	var hi = 0;
	var hi1 = a.hi;
	var hi2 = b.hi;
	var lo = 0;
	var lo1 = a.lo;
	var lo2 = b.lo;
	var base = 1;
	for (var i = 0; i < 32; i++) {
		if (hi1 %2 !== hi2 % 2) {
			hi += base;
		}
		if (lo1 %2 !== lo2 % 2) {
			lo += base;
		}
		base  *= 2;
		hi1 = Math.floor(hi1 / 2);
		hi2 = Math.floor(hi2 / 2);
		lo1 = Math.floor(lo1 / 2);
		lo2 = Math.floor(lo2 / 2);
	}
	return {
		hi: hi,
		lo: lo
	};
};
js2me.dataToFloat = function (value) {
	var sign = (value & 0x80000000) != 0;
	var exponent = ((value & 0x7f800000) >> 23) - 127;
	var fraction = (value & 0x007fffff);
	for (var j = 0; j < 23; j++) {
		fraction /= 2;
	}
	fraction += 1;
	while (exponent != 0) {
		if (exponent > 0) {
			fraction *= 2;
			exponent--;
		} else {
			fraction /= 2;
			exponent++;
		}
	}
	if (sign) {
		fraction *= -1;
	}
	return fraction;
};
js2me.dataToDouble = function (hiData, loData) {
	var sign = (hiData & 0x80000000) != 0;
	var exponent = ((hiData & 0x7ff00000) >> 20) - 1023;
	hiData = (hiData & 0x000fffff) * 0x100000000;
	var fraction = 1;
	for (var j = 0; j < 52; j++) {
		hiData /= 2;
		loData /= 2;
	}
	fraction += hiData;
	fraction += loData;
	while (exponent != 0) {
		if (exponent > 0) {
			fraction *= 2;
			exponent--;
		} else {
			fraction /= 2;
			exponent++;
		}
	}
	if (sign) {
		fraction *= -1;
	}
	return {double: fraction};
};
js2me.FPToBytes = function (value, exponentLength, fractionLength) {
	var bytes = [];
	bytes[0] = 0;
	if (value < 0) {
		bytes[0] += 128;
		value =  -value;
	}
	var exp = 0;
	while (value < 1) {
		value *= 2;
		exp--;
	}
	while (value >= 2) {
		value /= 2;
		exp++;
	}
	exp += Math.pow(2, exponentLength - 1) - 1;
	bytes[1] = exp % Math.pow(2, exponentLength - 7);
	bytes[0] += Math.floor(exp / Math.pow(2, exponentLength - 7));
	value -= 1;
	value *= Math.pow(2, 8 - ((1 + exponentLength) % 8));
	bytes[1] += Math.floor(value);
	value = value % 1;
	var length = Math.floor((exponentLength  + fractionLength + 1) / 8)
	for (var i = 2; i < length; i++) {
		value *= 256;
		bytes[i] = Math.floor(value);
		value = value % 1;
	}
	return bytes;
};
js2me.dconst0 = {double: 0};
js2me.dconst1 = {double: 1};
/**
 * Loads resources from given list of zip entries to local cache.
 * @param {array} entries Array of zip.Entry elements.
 * @param {function} callback Function to execute when all resources are loaded.
 */
js2me.loadResources = function (entries, callback) {
	var remain = entries.length;
	function finish() {
		remain--;
		if (remain == 0) {
			callback();
		}
	}
	var i = 0;
	function addResource(entry) {
		entry.getData(new zip.ArrayBufferWriter(), function (content) {
			document.getElementById('screen').innerHTML = 'Loading ' + entry.filename + ' (' + (i + 1) + '/' + entries.length + ')';
			js2me.resources[entry.filename] = new Uint8Array(content);
			finish();
			if (remain > 0) {
				addResource(entries[i]);
			}
		});
	}
	js2me.resources = {};
	addResource(entries[i]);
};
js2me.addResources = function (entries) {
	js2me.resources = {};
	for (var i in entries) {
		js2me.resources[entries[i].filename] = entries[i];
	}
};
js2me.loadResource = function (name, callback) {
	if (name.indexOf('/') === 0) {
		name = name.substring(1);
	}
	if (js2me.resources[name] instanceof Uint8Array) {
		callback(js2me.resources[name]);
	} else {
		if (js2me.resources[name] == null) {
			callback(null);
		} else {
			js2me.resources[name].getData(new zip.ArrayBufferWriter(), function (content) {
				js2me.resources[name] = new Uint8Array(content);
				callback(js2me.resources[name]);
			});
		}
	}
};
/**
 * Restores suspended thread with given id.
 * @param {number} threadId Id of some thread which is currently suspended.
 */
js2me.restoreThread = function (threadId) {
	js2me.lastStop = +new Date;
	if (js2me.kill) {
		return;
	}
	if (js2me.restoreStack[threadId] == undefined) {
		return;
	}
	js2me.isThreadSuspended = false;
	var restoreStack = js2me.restoreStack[threadId].pop();
	if (restoreStack) {
		js2me.switchVM(threadId);
		if (typeof restoreStack == 'function') {
			return restoreStack();
		} else {
			return js2me.execute.apply(js2me, restoreStack);
		}
	}
};
/**
 * Launch new thread.
 * @param {function} func Action to be executed.
 */
js2me.launchThread = function (func) {
	var threadId = -js2me.lastThread;
	js2me.VMMapping[threadId] = js2me.currentVM;
	js2me.lastThread++;
	if (!js2me.restoreStack[threadId]) {
		js2me.restoreStack[threadId] = [];
	}
	setZeroTimeout(function () {
		js2me.switchVM(threadId);
		try {
			js2me.isThreadSuspended = false;
			js2me.currentThread = threadId;
			func();
			js2me.isThreadSuspended = false;
		} catch (e) {
			console.error(e.stack);
			js2me.showError(e.message);
		}
	}, 1);
};
js2me.lastThread = 1;
js2me.enterMonitor = function (obj) {
	if (obj.monitorQueue == null) {
		obj.monitorQueue = [];
	}
	if (obj.monitorQueue.length === 0 || obj.monitorQueue[0] === js2me.currentThread) {
		obj.monitorQueue.unshift(js2me.currentThread)
	} else {
		obj.monitorQueue.push(js2me.currentThread)
		js2me.isThreadSuspended = true;
	}
};
js2me.exitMonitor = function (obj) {
	if (this.monitorQueue == null) {
		this.monitorQueue = [];
	}
	obj.monitorQueue.shift();
	if (obj.monitorQueue.length !== 0 && obj.monitorQueue[0] !== js2me.currentThread) {
		var threadId = obj.monitorQueue[0];
		if (threadId != null) {
			setTimeout(function () {
				js2me.restoreThread(threadId);
			}, 1);
		}
	}
};
js2me.suspendThread = function (func) {
	js2me.isThreadSuspended = true;
	if (func) {
		if (js2me.restoreStack[js2me.currentThread].length > 0) {
			console.error('Trying to overwrite a thread!');
		}
		js2me.restoreStack[js2me.currentThread] = [func];
	}
	return js2me.currentThread;
};
js2me.switchVM = function (threadId) {
	js2me.currentThread = threadId;
	js2me.currentVM = js2me.VMMapping[threadId];
	js2me.statics = js2me['statics' + js2me.currentVM];
};
js2me.currentVM;
js2me.VMMapping = {};
window.addEventListener('load', function () {
	if (js2me.config.workers) {
		js2me.worker = new Worker('js/worker.js');
		js2me.worker.postMessage(['setConfig', js2me.config]);
		var storage = {};
		for (var i in localStorage) {
			storage[i] = localStorage[i];
		}
		js2me.worker.postMessage(['setStorage', storage]);
		js2me.sharedObjects = [document.body];
		js2me.worker.onmessage = function (event) {
			var piece;
			var parent = window;
			var obj = window;
			var data = event.data.slice(0);
			if (data.length === 1) {
				js2me.sharedObjects[data[0].sharedId][data[0].name] = data[0].value;
				return;
			}
			if (data[0].constructor === Number) {
				obj = js2me.sharedObjects[data.shift()];
			}
			while ((piece = data.shift()).constructor === String) {
				parent = obj;
				obj = obj[piece];
			}
			for (var i = 0; i < data.length; i++) {
				if (data[i] && data[i].objectId != null) {
					data[i] = js2me.sharedObjects[data[i].objectId];
				}
				if (data[i] && data[i].functionId != null) {
					(function (id) {
						data[i] = function () {
							js2me.worker.postMessage(['fire', id]);
						}
					})(data[i].functionId);
				}
			}
			try {
				var result = obj.apply(parent, data);
			} catch (e) {
				debugger;
			}
			if (piece >= 0) {
				js2me.sharedObjects[piece] = result;
				if (result && result.tagName === 'IMG') {
					result.addEventListener('load', function () {
						js2me.worker.postMessage(['setProperties', piece, {width: result.width, height: result.height}]);
					});
				}
			}
		};
	}
});
/**
 * Converts given array into string.
 * @param {array} array Array of bytes which is correct UTF8 content.
 */
js2me.UTF8ToString = function (sourceArray, offset, length) {
	if (offset == null) {
		offset = 0;
	}
	var i = 0;
	if (length == null) {
		length = sourceArray.length;
	}
	var array = [];
	for (var j = 0; j < length; j++) {
		var code = sourceArray[offset + j];
		if (code < 0) {
			code += 256;
		}
		array[j] = code;
	}
	var result = [];
	while(i < length) {
		if (array[i] < 0x80) {
			var code = array[i];
			i++;
		} else if ((array[i] & 0xE0) == 0xC0) {
			var code = ((array[i] & 0x1F) << 6) | (array[i + 1] & 0x3F);
			i += 2;
		} else if ((array[i] & 0xF0) == 0xE0) {
			var code = (((array[i] & 0x0F) << 12) | ((array[i + 1] & 0x3F) << 6) | (array[i + 2] & 0x3F));
			i += 3;
		} else {
			return null;
		}
		
		var char = String.fromCharCode(code);
		if (char != '') {
			result.push(char);
		} else {
			return null;
		}
	}
	return result.join('');
};
js2me.stringToUTF8 = function (text) {
	var result = [];
	for (var i = 0; i < text.length; i++) {
		var char = text.charCodeAt(i);
		if (char >= 0x01 && char <= 0x007F) {
			result.push(char);
		}
		if (char == 0 || (char >= 0x0080 && char <= 0x07FF)) {
			result.push(0xC0 | (0x1F & (char >> 6)));
			result.push(0x80 | (0x3F & char));
		}
		if (char >= 0x0800 && char <= 0xFFFF) {
			result.push(0xE0 | (0x0F & (char >> 12)));
			result.push(0x80 | (0x3F & (char >>  6)));
			result.push(0x80 | (0x3F & char));
		}
	}
	return result;
};
js2me.bytesToDataURI = function (bytes, offset, length, mime) {
	var dataURI = 'data:' + mime + ',';
	for (var j = offset; j < offset + length; j++) {
		dataURI += '%';
		var code = bytes[j];
		if (code < 0) {
			code += 256;
		}
		code = code.toString(16)
		if (code.length == 1) {
			dataURI += '0';
		}
		dataURI += code;
	}
	return dataURI;
};

js2me.markUnsafe = function (func) {
	func.isUnsafe = true;
	return func;
};

// setZeroTimeout - L. David Baron <dbaron@dbaron.org>
var setZeroTimeout = (function() {
	var timeouts = [];
	var messageName = "zero-timeout-message";

	// Like setTimeout, but only takes a function argument.  There's
	// no time argument (always zero) and no arguments (you have to
	// use a closure).
	function setZeroTimeout(fn) {
	  timeouts.push(fn);
	  window.postMessage(messageName, "*");
	}

	function handleMessage(event) {
	  if (event.source == window && event.data == messageName) {
		event.stopPropagation();
		if (timeouts.length > 0) {
		  var fn = timeouts.shift();
		  fn();
		}
	  }
	}

	window.addEventListener("message", handleMessage, true);

	setZeroTimeout.removeListener = function () {
	  window.removeEventListener('message', handleMessage, true);  
	}

	// Add the one thing we want added to the window object.
	return setZeroTimeout;
})();
